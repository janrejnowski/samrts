<?php

namespace Tests;

use JanRejnowski\SamRts\App\Enums\Action;
use JanRejnowski\SamRts\App\Model\RtsContainer;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class ContainersApiTest extends ApiCrudTest
{
    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $endpoint = 'rts/containers';
    protected string $model = RtsContainer::class;
    private string $badUuid = 'bc02efc8-232f-4c05-a39e-871837a1ec39';

    protected array $additionalFields = [
        'serial_number' => null
    ];

    protected array $withoutFields = [
        'weight'
    ];

    protected function createModel(array $attributes = []): RtsContainer
    {
        return factory($this->model)->create($attributes);
    }

    public function test_get_all(): void
    {
        $model = $this->createModel();
        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uuid',
                        'name',
                        'weight',
                        'capacity',
                        'serial_number',
                        'description'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'weight' => $model->weight,
                'capacity' => $model->capacity,
                'serial_number' => $model->serial_number,
                'description' => $model->description
            ])
            ->assertJsonCount(1, 'data');
    }

    public function test_get_one(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'uuid',
                    'name',
                    'weight',
                    'capacity',
                    'serial_number',
                    'description'
                ]
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'weight' => $model->weight,
                'capacity' => $model->capacity,
                'serial_number' => $model->serial_number,
                'description' => $model->description
            ]);
    }

    public function test_get_not_found(): void
    {
        $this->get("/api/$this->endpoint/$this->badUuid")
            ->assertNotFound()
            ->assertJsonFragment([
                'code' => 404,
                'message' => __('Given uuid :code is invalid or container not exist.', ['code' => $this->badUuid])
            ]);
    }

    public function test_forbidden_endpoint_get_one(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_store(): void
    {
        $model = $this->modelForStore();

        $this->post("/api/$this->endpoint", $model)
            ->assertCreated()
            ->assertJsonFragment([
                'name' => $model['name'],
                'weight' => $model['weight'],
                'capacity' => $model['capacity'],
                'serial_number' => $model['serial_number'],
                'description' => $model['description']
            ]);
    }

    public function test_failed_validation_store(): void
    {
        $this->post("/api/$this->endpoint/", $this->arrayModel(true))
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'weight'
                    ],
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute field is required.', ['attribute' => 'weight'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_store_unique_name_and_serial_number()
    {
        $model = $this->createModel([
            'serial_number' => 'test'
        ]);
        $new = $model->toArray();

        $this->post("/api/$this->endpoint/", $new)
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'name' => [
                        __('The :attribute has already been taken.', ['attribute' => 'name'])
                    ],
                    'serial_number' => [
                        __('The :attribute has already been taken.', ['attribute' => 'serial number'])
                    ]
                ]
            ]);
    }

    public function test_validation_store_unique_nullable_serial_number()
    {
        $model = $this->createModel(['serial_number' => null]);
        $new = $this->modelForStore();

        self::assertNull($model->serial_number);
        self::assertNull($new['serial_number']);
        $this->post("/api/$this->endpoint/", $new)
            ->assertCreated();
    }

    public function test_replace(): void
    {
        $model = $this->createModel();
        $new = $this->modelForStore();

        $this->put("/api/$this->endpoint/$model->uuid", $new)
            ->assertOk()
            ->assertJsonFragment([
                'name' => $new['name'],
                'weight' => $new['weight'],
                'capacity' => $new['capacity'],
                'serial_number' => $new['serial_number'],
                'description' => $new['description']
            ]);
    }

    public function test_endpoint_put_replace(): void
    {
        $model = $this->createModel();

        $this->put("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_put_replace(): void
    {
        $this->assertForbiddenCall('put', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_destroy(): void
    {
        $model = $this->createModel();

        $this->delete("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'weight' => $model->weight,
                'capacity' => $model->capacity,
                'serial_number' => $model->serial_number,
                'description' => $model->description
            ]);
    }

    public function test_endpoint_delete_destroy(): void
    {
        $model = $this->createModel();

        $this->delete("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_delete_destroy(): void
    {
        $this->assertForbiddenCall('delete', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_forbidden_endpoint_get_form_data(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint/$this->badUuid/form-data");
    }

    public function test_endpoint_get_form_data_not_found(): void
    {
        $this->get("/api/$this->endpoint/$this->badUuid/form-data")
            ->assertNotFound()
            ->assertJsonFragment([
                'code' => 404,
                'message' => __('Given uuid :code is invalid or container not exist.', ['code' => $this->badUuid])
            ]);
    }

    public function test_endpoint_get_form_data_without_action(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint/$model->uuid/form-data")
            ->assertJsonFragment([
                'marker_uuid' => null,
                'last_action' => null,
                'next_action' => Action::InTent
            ]);
    }

    public function test_endpoint_get_form_data_with_action(): void
    {

    }
}

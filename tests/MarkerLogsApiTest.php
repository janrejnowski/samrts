<?php

use Illuminate\Foundation\Testing\WithFaker;
use JanRejnowski\SamRts\App\Enums\Action;
use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;
use JanRejnowski\SamRts\App\Model\RtsContainer;
use JanRejnowski\SamRts\App\Model\RtsLocation;
use JanRejnowski\SamRts\App\Model\RtsMarker;
use JanRejnowski\SamRts\App\Model\RtsMarkerLog;
use JanRejnowski\SamRts\App\Model\RtsWasteType;
use Totem\SamAcl\Testing\AssertForbiddenCall;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;

class MarkerLogsApiTest extends ApiTest
{
    use AttachRoleToUserTrait,
        AssertForbiddenCall,
        WithFaker;

    protected string $endpoint = 'rts/logs';
    protected string $model = RtsMarkerLog::class;
    private string $badUuid = 'bc02efc8-232f-4c05-a39e-871837a1ec39';

    protected function createModel(array $attributes = []): RtsMarkerLog
    {
        return factory($this->model)->make($attributes);
    }

    private function bindRtsMarkerAssociations(RtsMarker $marker, RtsLocation $location = null, RtsWasteType $waste_type = null): void
    {
        if ($location !== null) {
            $marker->location()->associate($location);
        }
        if ($waste_type !== null) {
            $marker->waste_type()->associate($waste_type);
        }
    }

    private function createRtsMarkerModel(array $attributes = [], RtsLocation $location = null, RtsWasteType $waste_type = null): RtsMarker
    {
        $marker = new RtsMarker($attributes);

        $this->bindRtsMarkerAssociations($marker, $location, $waste_type);

        $marker->save();

        return $marker;
    }

    public function test_endpoint_get_all(): void
    {
        $this->get("/api/$this->endpoint")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_get_all(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint");
    }

    public function test_get_all(): void
    {
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $rtsContainer = $this->getRandomContainer();
        $this->createLog(Action::InTent, $rtsContainer);
        $rtsMarker = $this->createRtsMarkerModel([], $location, $waste_type);
        $this->createLog(Action::InHall, $rtsContainer, $rtsMarker);
        $this->createLog(Action::ToWeighting, $rtsContainer, $rtsMarker);
        $this->createLog(Action::Weighting, $rtsContainer, $rtsMarker);
        $logWeighting = $this->createLog(Action::Weighting, $rtsContainer, $rtsMarker, $rtsMarker);

        $this->get("/api/$this->endpoint")
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uuid',
                        'container_uuid',
                        'marker_uuid',
                        'action',
                        'user_id',
                        'weight',
                        'correct_marker_uuid',
                        'description',
                        'category',
                        'price',
                        'cost'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'action' => Action::getKey(Action::InTent),
                'container_uuid' => $rtsContainer->uuid,
            ])
            ->assertJsonFragment([
                'action' => Action::getKey(Action::InHall),
                'container_uuid' => $rtsContainer->uuid,
                'marker_uuid' => $rtsMarker->uuid
            ])
            ->assertJsonFragment([
                'action' => Action::getKey(Action::ToWeighting),
                'container_uuid' => $rtsContainer->uuid,
                'marker_uuid' => $rtsMarker->uuid
            ])
            ->assertJsonFragment([
                'action' => Action::getKey(Action::Weighting),
                'container_uuid' => $rtsContainer->uuid,
                'marker_uuid' => $rtsMarker->uuid,
                'category' => $rtsMarker->waste_type->category,
                'price' => $logWeighting->price,
                'cost' => $logWeighting->cost,
                'weight' => $logWeighting->weight,
                'user_id' => $logWeighting->user_id
            ])
            ->assertJsonFragment([
                'action' => Action::getKey(Action::Weighting),
                'container_uuid' => $rtsContainer->uuid,
                'marker_uuid' => $rtsMarker->uuid,
                'correct_marker_uuid' => $rtsMarker->uuid,
                'category' => $rtsMarker->waste_type->category,
                'price' => $logWeighting->price,
                'cost' => $logWeighting->cost,
                'weight' => $logWeighting->weight,
                'user_id' => $logWeighting->user_id
            ]);
    }

    public function test_forbidden_endpoint_post_store_without_action(): void
    {
        $this->assertForbiddenCall('post', "/api/$this->endpoint");
    }

    public function test_failed_validation_null_action_store_with_action(): void
    {
        $payload = [
            'action' => null
        ];
        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => __('Action not recognized.')
            ]);
    }

    public function test_failed_validation_non_existent_action_store_with_action():void
    {
        $payload = [
            'action' => 100
        ];
        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => __('Action not recognized.')
            ]);
    }

    public function test_store_with_action_in_tent(): void
    {
        $action = Action::getKey(Action::InTent);
        $container = $this->getRandomContainer();

        $payload = [
            'container_uuid' => $container->uuid,
            'action' => Action::InTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'action' => $action
            ]);
    }

    public function test_failed_validation_null_container_uuid_store_with_action_in_tent(): void
    {
        $payload = [
            'container_uuid' => null,
            'action' => Action::InTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'container uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_non_existent_container_store_with_action_in_tent(): void
    {
        $payload = [
            'container_uuid' => $this->badUuid,
            'action' => Action::InTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'container uuid'])
                    ]
                ]
            ]);
    }

    public function test_store_with_action_in_hall(): void
    {
        $action = Action::getKey(Action::InHall);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);

        $payload = [
          'container_uuid' => $container->uuid,
          'marker_uuid' => $marker->uuid,
          'action' => Action::InHall
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => $action
            ]);
    }

    public function test_failed_validation_null_container_uuid_marker_uuid_store_with_action_in_hall(): void
    {
        $payload = [
            'container_uuid' => null,
            'marker_uuid' => null,
            'action' => Action::InHall
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid',
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'container uuid'])
                    ],
                    'marker_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_non_existent_container_marker_store_with_action_in_hall(): void
    {
        $payload = [
            'container_uuid' => $this->badUuid,
            'marker_uuid' => $this->badUuid,
            'action' => Action::InHall
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid',
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'container uuid'])
                    ],
                    'marker_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_store_with_action_to_weighting(): void
    {
        $action = Action::getKey(Action::ToWeighting);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'action' => Action::ToWeighting
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => $action
            ]);
    }

    public function test_failed_validation_null_container_uuid_marker_uuid_store_with_action_to_weighting(): void
    {
        $payload = [
            'container_uuid' => null,
            'marker_uuid' => null,
            'action' => Action::InHall
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid',
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'container uuid'])
                    ],
                    'marker_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_non_existent_container_uuid_marker_uuid_store_with_action_to_weighting(): void
    {
        $payload = [
            'container_uuid' => $this->badUuid,
            'marker_uuid' => $this->badUuid,
            'action' => Action::InHall
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid',
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'container uuid'])
                    ],
                    'marker_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_store_with_action_weighting_without_correct_marker(): void
    {
        $action = Action::getKey(Action::Weighting);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);

        $collection = collect([]);

        for ($i = 0; $i < 10; $i++) {
            if ($i < 5) {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date,
                    'category' => $waste_type->category
                ]));
            } else {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date
                ]));
            }
        }

        $category_price = $collection->where('category', $waste_type->category)->sortByDesc('created_at')->first();

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action',
                    'weight',
                    'category',
                    'price',
                    'correct_marker_uuid',
                    'description',
                    'user_id',
                    'price',
                    'cost',
                    'category'
                ]
            ])
            ->assertJsonFragment([
               'container_uuid' => $container->uuid,
               'marker_uuid' => $marker->uuid,
               'action' => $action,
               'weight' => $weight,
               'category' => $waste_type->category,
               'cost' => $waste_type->cost,
               'price' => $category_price->price,
               'user_id' => auth()->id(),
               'correct_marker_uuid' => null,
               'description' => null
            ]);
    }

    public function test_failed_validation_incorrect_weight_value_store_with_action_weighting_without_correct_marker(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);

        $weight =$this->faker->numberBetween(-1, -100);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be at least :min.', ['attribute' => 'weight', 'min' => 1])
                    ]
                ]
            ]);

        $this->withToken()->post("/api/$this->endpoint/action", array_merge($payload, ['weight' => 'test']))
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be an integer.', ['attribute' => 'weight'])
                    ]
                ]
            ]);

        $this->withToken()->post("/api/$this->endpoint/action", array_merge($payload, ['weight' => $this->faker->randomFloat(null, 1)]))
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be an integer.', ['attribute' => 'weight'])
                    ]
                ]
            ]);
    }

    public function test_store_with_action_weighting_with_correct_marker(): void
    {
        $action = Action::getKey(Action::Weighting);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);

        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $correct_marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $image = \Illuminate\Http\UploadedFile::fake()->image('dump.jpg');

        $collection = collect([]);

        for ($i = 0; $i < 10; $i++) {
            if ($i < 5) {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date(),
                    'category' => $waste_type->category
                ]));
            } else {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date()
                ]));
            }
        }

        $category_price = $collection->where('category', $waste_type->category)->sortByDesc('created_at')->first();
        $test_description = $this->faker->text;

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
            'correct_marker_uuid' => $correct_marker->uuid,
            'description' => $test_description,
            'image' => $image
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action',
                    'weight',
                    'category',
                    'price',
                    'correct_marker_uuid',
                    'description',
                    'user_id',
                    'price',
                    'cost',
                    'category',
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => $action,
                'weight' => $weight,
                'category' => $waste_type->category,
                'cost' => $waste_type->cost,
                'price' => $category_price->price,
                'user_id' => auth()->id()
            ]);
    }

    public function test_failed_validation_incorrect_weight_value_store_with_action_weighting_with_correct_marker(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight =$this->faker->numberBetween(-1, -100);
        $test_description = $this->faker->text;
        $image = \Illuminate\Http\UploadedFile::fake()->image('dump.jpg');

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'correct_marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
            'description' => $test_description,
            'image' => $image
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be at least :min.', ['attribute' => 'weight', 'min' => 1])
                    ]
                ]
            ]);

        $this->withToken()->post("/api/$this->endpoint", array_merge($payload, ['weight' => 'test']))
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be an integer.', ['attribute' => 'weight'])
                    ]
                ]
            ]);

        $this->withToken()->post("/api/$this->endpoint", array_merge($payload, ['weight' => $this->faker->randomFloat(null, 1)]))
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'weight' => [
                        __('The :attribute must be an integer.', ['attribute' => 'weight'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_without_description_image_store_with_action_weighting_with_correct_marker(): void
    {
        $action = Action::getKey(Action::Weighting);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);

        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $correct_marker = $this->createRtsMarkerModel([], $location, $waste_type);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
            'correct_marker_uuid' => $correct_marker->uuid,
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'description' => [
                        __('The :attribute field is required when :values is present.', ['attribute' => 'description', 'values' => 'correct marker uuid'])
                    ],
                    'image' => [
                        __('The :attribute field is required when :values is present.', ['attribute' => 'image', 'values' => 'correct marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_wrong_file_store_with_action_weighting_with_correct_marker(): void
    {
        $action = Action::getKey(Action::Weighting);
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);

        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $correct_marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $image = \Illuminate\Http\UploadedFile::fake()->create('test.txt');
        $test_description = $this->faker->text;

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
            'action' => Action::Weighting,
            'correct_marker_uuid' => $correct_marker->uuid,
            'description' => $test_description,
            'image' => $image
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message'
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'image' => [
                        __('The :attribute must be an image.', ['attribute' => 'image'])
                    ]
                ]
            ]);
    }

    public function test_store_with_action_out_tent(): void
    {
        $action = Action::getKey(Action::OutTent);
        $container = $this->getRandomContainer();

        $payload = [
            'container_uuid' => $container->uuid,
            'action' => Action::OutTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'action' => $action
            ]);
    }

    public function test_failed_validation_null_container_uuid_store_with_action_out_tent(): void
    {
        $payload = [
            'container_uuid' => null,
            'action' => Action::OutTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'container uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_non_existent_container_store_with_action_out_tent(): void
    {
        $payload = [
            'container_uuid' => $this->badUuid,
            'action' => Action::OutTent
        ];

        $this->post("/api/$this->endpoint/action", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'container uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_non_existent_container_store_without_action(): void
    {
        $payload = [
            'container_uuid' => $this->badUuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->dump()
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'container_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'container_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'container uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_no_logs_for_container_store_without_action(): void
    {
        $container_uuid = $this->getRandomContainer()->uuid;

        $payload = [
            'container_uuid' => $container_uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
               'container_uuid' => $container_uuid,
               'action' => Action::getKey(Action::InTent)
            ]);
    }

    public function test_store_validate_last_action_in_tent(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $this->createLog(Action::InTent, $container);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => Action::getKey(Action::InHall)
            ]);
    }

    public function test_failed_validation_null_marker_uuid_store_validate_last_action_in_tent(): void
    {
        $container = $this->getRandomContainer();
        $this->createLog(Action::InTent, $container);

        $payload = [
            'container_uuid' => $container->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'marker_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_bad_marker_uuid_store_validate_last_action_in_tent(): void
    {
        $container = $this->getRandomContainer();
        $this->createLog(Action::InTent, $container);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $this->badUuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'marker_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_store_validate_last_action_in_hall(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $this->createLog(Action::InHall, $container);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => Action::getKey(Action::ToWeighting)
            ]);
    }

    public function test_failed_validation_null_marker_uuid_store_validate_last_action_in_hall(): void
    {
        $container = $this->getRandomContainer();
        $this->createLog(Action::InTent, $container);

        $payload = [
            'container_uuid' => $container->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'marker_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_bad_marker_uuid_store_validate_last_action_in_hall(): void
    {
        $container = $this->getRandomContainer();
        $this->createLog(Action::InHall, $container);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $this->badUuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'marker_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'marker_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'marker uuid'])
                    ]
                ]
            ]);
    }

    public function test_store_validate_last_action_to_weighting_no_correct_marker(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);
        $this->createLog(Action::ToWeighting, $container);

        $collection = collect([]);

        for ($i = 0; $i < 10; $i++) {
            if ($i < 5) {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date,
                    'category' => $waste_type->category
                ]));
            } else {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date
                ]));
            }
        }

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'weight' => $weight,
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'weight'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => Action::getKey(Action::Weighting)
            ]);
    }

    public function test_store_validate_last_action_to_weighting_correct_marker(): void
    {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $correct_marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $weight = $this->faker->randomNumber(3);
        $this->createLog(Action::ToWeighting, $container);

        $image = \Illuminate\Http\UploadedFile::fake()->image('dump.jpg');
        $description = $this->faker->text;

        $collection = collect([]);

        for ($i = 0; $i < 10; $i++) {
            if ($i < 5) {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date(),
                    'category' => $waste_type->category
                ]));
            } else {
                $collection->push($this->createCategoryPriceModel([
                    'created_at' => $this->faker->date()
                ]));
            }
        }

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
            'correct_marker_uuid' => $correct_marker->uuid,
            'weight' => $weight,
            'description' => $description,
            'image' => $image
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid',
                    'correct_marker_uuid',
                    'weight'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => Action::getKey(Action::Weighting)
            ]);
    }

    public function test_store_validate_last_action_weighting(): void {
        $container = $this->getRandomContainer();
        $location = $this->getRandomLocation();
        $waste_type = $this->getRandomWasteType();
        $marker = $this->createRtsMarkerModel([], $location, $waste_type);
        $correct_marker = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->createLog(Action::Weighting, $container, $marker, $correct_marker);

        $payload = [
            'container_uuid' => $container->uuid,
            'marker_uuid' => $marker->uuid,
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'marker_uuid'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'marker_uuid' => $marker->uuid,
                'action' => Action::getKey(Action::OutTent)
            ]);
    }

    public function test_store_without_action_last_action_out_tent(): void
    {
        $container = $this->getRandomContainer();
        $this->createLog(Action::OutTent, $container);

        $payload = [
            'container_uuid' => $container->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'container_uuid',
                    'action'
                ]
            ])
            ->assertJsonFragment([
                'container_uuid' => $container->uuid,
                'action' => Action::getKey(Action::InTent)
            ]);

    }

    private function createCategoryPriceModel(array $attributes = []): RtsCategoryPrice
    {
        return factory(RtsCategoryPrice::class)->create($attributes);
    }

    private function getRandomContainer(): RtsContainer
    {
        return factory(RtsContainer::class, 3)->create()->random();
    }

    private function getRandomLocation(): RtsLocation
    {
        return factory(RtsLocation::class, 3)->create()->random();
    }

    private function getRandomWasteType(): RtsWasteType
    {
        return factory(RtsWasteType::class, 2)->create()->random();
    }

    private function createLog(int $action, RtsContainer $container, RtsMarker $marker = null, RtsMarker $correct_marker = null): RtsMarkerLog
    {
        $log = new RtsMarkerLog();
        $log->action = Action::getKey($action);
        $log->container()->associate($container);
        if ($marker !== null) {
            $log->marker()->associate($marker);
        }
        if ($correct_marker !== null) {
            $log->correct_marker()->associate($correct_marker);
            $log->description = $this->faker->text;
        }
        if ($log->action === Action::getKey(Action::Weighting)) {
            $log->weight = $this->faker->randomNumber(3);
            $log->category = $marker->waste_type->category;
            $log->price = $this->faker->randomFloat(2, 1, 50);
            $log->cost = $this->faker->randomFloat(2, 1, 50);
            $log->user_id = auth()->id();
        }

        $log->save();

        return $log;
    }
}

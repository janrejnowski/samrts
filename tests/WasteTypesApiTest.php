<?php

namespace Tests;

use JanRejnowski\SamRts\App\Model\RtsWasteType;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class WasteTypesApiTest extends ApiCrudTest
{
    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $endpoint = 'rts/wastes';
    protected string $model = RtsWasteType::class;
    private string $badUuid = 'bc02efc8-232f-4c05-a39e-871837a1ec39';

    protected array $withoutFields = [
        'cost'
    ];

    protected function createModel(array $attributes = []): RtsWasteType
    {
        return factory($this->model)->create($attributes);
    }

    public function test_get_all(): void
    {
        $model = $this->createModel();
        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uuid',
                        'name',
                        'description',
                        'category',
                        'cost'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'description' => $model->description,
                'category' => $model->category,
                'cost' => $model->cost
            ])
            ->assertJsonCount(1, 'data');
    }

    public function test_get_one(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'uuid',
                    'name',
                    'description',
                    'category',
                    'cost'
                ]
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'description' => $model->description,
                'category' => $model->category,
                'cost' => $model->cost
            ]);
    }

    public function test_get_not_found(): void
    {
        $this->get("/api/$this->endpoint/$this->badUuid")
            ->assertNotFound()
            ->assertJsonFragment([
                'code' => 404,
                'message' => __('Given uuid :code is invalid or waste type not exist.', ['code' => $this->badUuid])
            ]);
    }

    public function test_forbidden_endpoint_get_one(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_store(): void
    {
        $model = $this->modelForStore();
        $this->post("/api/$this->endpoint", $model)
            ->assertCreated()
            ->assertJsonFragment([
                'name' => $model['name'],
                'description' => $model['description'],
                'category' => $model['category'],
                'cost' => $model['cost'],
            ]);
    }

    public function test_failed_validation_store(): void
    {
        $this->post("/api/$this->endpoint/", $this->arrayModel(true))
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'cost'
                    ],
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'cost' => [
                        __('The :attribute field is required.', ['attribute' => 'cost'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_store_unique_name()
    {
        $model = $this->createModel();
        $new = $model->toArray();

        $this->post("/api/$this->endpoint/", $new)
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'name' => [
                        __('The :attribute has already been taken.', ['attribute' => 'name'])
                    ]
                ]
            ]);
    }

    public function test_replace(): void
    {
        $model = $this->createModel();
        $new = $this->modelForStore();

        $this->put("/api/$this->endpoint/$model->uuid", $new)
            ->assertOk()
            ->assertJsonFragment([
                'name' => $new['name'],
                'description' => $new['description'],
                'category' => $new['category'],
                'cost' => $new['cost']
            ]);
    }

    public function test_endpoint_put_replace(): void
    {
        $model = $this->createModel();

        $this->put("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_put_replace(): void
    {
        $this->assertForbiddenCall('put', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_destroy(): void
    {
        $model = $this->createModel();

        $this->delete("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'name' => $model->name,
                'description' => $model->description,
                'category' => $model->category,
                'cost' => $model->cost
            ]);
    }

    public function test_endpoint_delete_destroy(): void
    {
        $model = $this->createModel();

        $this->delete("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_delete_destroy(): void
    {
        $this->assertForbiddenCall('delete', "/api/$this->endpoint/$this->badUuid");
    }

}

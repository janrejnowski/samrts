<?php

use JanRejnowski\SamRts\App\Model\RtsLocation;
use JanRejnowski\SamRts\App\Model\RtsMarker;
use JanRejnowski\SamRts\App\Model\RtsWasteType;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class MarkersApiTest extends ApiCrudTest
{
    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $endpoint = 'rts/markers';
    protected string $model = RtsMarker::class;
    private string $badUuid = 'bc02efc8-232f-4c05-a39e-871837a1ec39';

    protected array $withoutFields = [
        'location_uuid'
    ];

    protected function createModel(array $attributes = []): RtsMarker
    {
        return new RtsMarker($attributes);
    }

    private function bindRtsMarkerAssociations(RtsMarker $marker, RtsLocation $location = null, RtsWasteType $waste_type = null): void
    {
        if ($location === null && $waste_type === null) {
            return;
        }
        if ($location !== null) {
            $marker->location()->associate($location);
        }
        if ($waste_type !== null) {
            $marker->waste_type()->associate($waste_type);
        }
    }

    private function createRtsMarkerModel(array $attributes = [], RtsLocation $location = null, RtsWasteType $waste_type = null): RtsMarker
    {
        $marker = new RtsMarker($attributes);

        $this->bindRtsMarkerAssociations($marker, $location, $waste_type);

        $marker->save();

        return $marker;
    }

    public function test_get_all(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uuid',
                        'location_uuid',
                        'waste_type_uuid'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'location_uuid' => $location->uuid,
                'waste_type_uuid' => $waste_type->uuid
            ]);
    }

    public function test_get_one(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->get("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'uuid',
                    'location_uuid',
                    'location',
                    'waste_type_uuid',
                    'waste_type'
                ]
            ])
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'location_uuid' => $location->uuid,
                'location' => $location->name,
                'waste_type_uuid' => $waste_type->uuid,
                'waste_type' => $waste_type->name
            ]);
    }

    public function test_get_not_found(): void
    {
        $this->get("/api/$this->endpoint/$this->badUuid")
            ->assertNotFound()
            ->assertJsonFragment([
                'code' => 404,
                'message' => __('Given uuid :code is invalid or marker not exist.', ['code' => $this->badUuid])
            ]);
    }

    public function test_forbidden_endpoint_get_one(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_store(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $payload = [
            'location_uuid' => $location->uuid,
            'waste_type_uuid' => $waste_type->uuid
        ];

        $this->post("/api/$this->endpoint", $payload)
            ->assertCreated()
            ->assertJsonFragment([
                'location_uuid' => $location->uuid,
                'waste_type_uuid' => $waste_type->uuid
            ]);
    }

    public function test_failed_validation_store(): void
    {
        $this->post("/api/$this->endpoint/", [])
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'location_uuid',
                        'waste_type_uuid'
                    ],
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'location_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'location uuid'])
                    ],
                    'waste_type_uuid' => [
                        __('The :attribute field is required.', ['attribute' => 'waste type uuid'])
                    ]
                ]
            ]);
    }

    public function test_failed_validation_store_exists_location_uuid_waste_type_uuid()
    {
        $model = $this->createModel([
            'location_uuid' => $this->badUuid,
            'waste_type_uuid' => $this->badUuid
        ]);

        $this->post("/api/$this->endpoint/", $model->attributesToArray())
            ->assertStatus(422)
            ->assertJsonFragment([
                'code' => 422,
                'message' => [
                    'location_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'location uuid'])
                    ],
                    'waste_type_uuid' => [
                        __('The selected :attribute is invalid.', ['attribute' => 'waste type uuid'])
                    ],
                ]
            ]);
    }

    public function test_endpoint_post_store(): void
    {
        $this->call('POST', "/api/$this->endpoint", [])->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_replace(): void
    {

        /** @var RtsLocation $location
         * @var RtsLocation $new_location
         */
        $locations = factory(RtsLocation::class, 3)->create();
        $location = $locations->random();
        $new_location = $locations->random();
        /** @var RtsWasteType $waste_type
         * @var RtsWasteType $new_waste_type
         */
        $waste_types = factory(RtsWasteType::class, 3)->create();
        $waste_type = $waste_types->random();
        $new_waste_type = $waste_types->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $payload = [
            'location_uuid' => $new_location->uuid,
            'waste_type_uuid' => $new_waste_type->uuid
        ];

        $this->put("/api/$this->endpoint/$model->uuid", $payload)
            ->assertOk()
            ->assertJsonFragment([
                'location_uuid' => $new_location->uuid,
                'waste_type_uuid' => $new_waste_type->uuid
            ]);
    }

    public function test_endpoint_put_replace(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->put("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_put_replace(): void
    {
        $this->assertForbiddenCall('put', "/api/$this->endpoint/$this->badUuid");
    }

    public function test_destroy(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->delete("/api/$this->endpoint/$model->uuid")
            ->assertOk()
            ->assertJsonFragment([
                'uuid' => $model->uuid,
                'location_uuid' => $model->location_uuid,
                'waste_type_uuid' => $model->waste_type_uuid,
            ]);
    }

    public function test_endpoint_delete_destroy(): void
    {
        /** @var RtsLocation $location */
        $location = factory(RtsLocation::class, 3)->create()->random();
        /** @var RtsWasteType $waste_type */
        $waste_type = factory(RtsWasteType::class, 2)->create()->random();

        $model = $this->createRtsMarkerModel([], $location, $waste_type);

        $this->delete("/api/$this->endpoint/$model->uuid")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_forbidden_endpoint_delete_destroy(): void
    {
        $this->assertForbiddenCall('delete', "/api/$this->endpoint/$this->badUuid");
    }

}

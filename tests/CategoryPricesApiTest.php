<?php

namespace Tests;

use JanRejnowski\SamRts\App\Enums\Category;
use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;
use Totem\SamAcl\Testing\AssertForbiddenCall;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;

class CategoryPricesApiTest extends ApiTest
{
    use AttachRoleToUserTrait,
        AssertForbiddenCall;

    protected string $endpoint = 'rts/prices';
    protected string $model = RtsCategoryPrice::class;

    protected array $withoutFields = [
        'price'
    ];

    protected function createModel(array $attributes = []): RtsCategoryPrice
    {
        return factory($this->model)->create($attributes);
    }

    public function test_get_all(): void
    {
        $collection = collect([]);

        for ($i = 0; $i < 10; $i++) {
            $collection->push($this->createModel([
                'created_at' => $this->getRandomDate()
            ]));
        }

        $categories = $collection->map(function ($element) {
            return $element->category;
        })->unique();

        $response = $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'category',
                        'price',
                        'created_at',
                    ]
                ],
                'apiVersion'
            ]);

        $categories->map(function ($category) use ($collection, $response) {
            $latest_category_date = $collection->where('category', $category)->max('created_at');

            $model = $collection->where('created_at', '=', $latest_category_date)->first();

            $response->assertJsonFragment([
                'category' => $category,
                'price' => $model->price,
                'created_at' => $model->created_at->format('Y-m-d H:i:s')
            ]);
        });
    }

    public function test_forbidden_endpoint_get_all(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint");
    }

    public function test_endpoint_get_all(): void
    {
        $this->get("/api/$this->endpoint")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_store(): void
    {
        $model = $this->modelForStore();

        $this->post("/api/$this->endpoint/", $model)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'category',
                    'price',
                    'created_at',
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'category' => $model['category'],
                'price' => $model['price'],
            ]);
    }

    public function test_forbidden_endpoint_post_store(): void
    {
        $this->assertForbiddenCall('post', "/api/$this->endpoint");
    }

    public function test_endpoint_post_store(): void
    {
        $this->post("/api/$this->endpoint", $this->modelForStore())->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_failed_validation_store(): void
    {
        $this->post("/api/$this->endpoint/", $this->arrayModel(true))
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message' => [
                        'price'
                    ],
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'code' => 422,
                "message" => [
                    "price" => [
                        __('The :attribute field is required.', ['attribute' => 'price'])
                    ]
                ]
            ]);
    }

    public function test_authorization_failed(): void
    {
        $this->withoutToken()->get("/api/$this->endpoint")
            ->assertJson([
                'error' => [
                    'message' => 'The token could not be parsed from the request'
                ]
            ])
            ->assertStatus(400);
    }

    public function test_get_all_categories(): void
    {
        $categories = Category::getInstances();

        $response = $this->get("/api/$this->endpoint/categories")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'text',
                        'value',
                    ]
                ],
                'apiVersion'
            ]);

        foreach ($categories as $category) {
            $response->assertJsonFragment([
                'text' => $category->description,
                'value' => $category->value,
            ]);
        }
    }

    public function test_forbidden_endpoint_get_all_categories(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint");
    }

    public function test_endpoint_get_all_categories(): void
    {
        $this->get("/api/$this->endpoint/categories")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function test_get_category_history(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint/$model->category/history")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'price',
                        'category',
                        'created_at'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'category' => $model->category,
                'price' => $model->price,
                'created_at' => $model->created_at->format('Y-m-d H:i:s'),
            ])
            ->assertJsonCount(1, 'data');
    }

    public function test_endpoint_get_category_history(): void
    {
        $categories = Category::getInstances();

        foreach ($categories as $category) {
            $this->get("/api/$this->endpoint/$category->value/history")->assertDontSee('"code":404')->assertDontSee('"code":405');
        }
    }

    public function test_forbidden_endpoint_get_category_history(): void
    {
        $category = Category::WhiteWastePaper;

        $this->assertForbiddenCall('get', "/api/$this->endpoint/$category/history");
    }

    public function test_get_history_for_non_exist_category(): void
    {
        $this->get("/api/$this->endpoint/0/history")
            ->assertStatus(400)
            ->assertJsonFragment([
                'error' => [
                    'code' => 400,
                    'message' => __("No category id have been given.")
                ]
            ]);
    }

    private function getRandomDate()
    {
        $int = mt_rand(1, 1262055681);

        return date("Y-m-d H:i:s", $int);
    }

}

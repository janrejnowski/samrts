Sam RTS Component
================

This package manages recycles tracking in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)
- [SAM-acl ~1.*](https://bitbucket.org/rudashi/samacl)
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin/)
- [SAM-Assets ~1.*](https://bitbucket.org/rudashi/samassets)
- [SAM-Users-Bundle ~1.*](https://bitbucket.org/rudashi/samusersbundle)
- [SAM-Users-Optima ~1.*](https://bitbucket.org/rudashi/samusersoptima)

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require janrejnowski/samrts
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/janrejnowski/samrts.git"
    }
],
```

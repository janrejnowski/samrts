<?php

use Illuminate\Support\Facades\Route;
use JanRejnowski\SamRts\App\Controllers\RtsCategoryPriceController;
use JanRejnowski\SamRts\App\Controllers\RtsContainerController;
use JanRejnowski\SamRts\App\Controllers\RtsLocationController;
use JanRejnowski\SamRts\App\Controllers\RtsMarkerController;
use JanRejnowski\SamRts\App\Controllers\RtsMarkerLogController;
use JanRejnowski\SamRts\App\Controllers\RtsWasteTypeController;
use JanRejnowski\SamRts\App\Middleware\LogRequestMiddleware;

Route::group(['prefix' => 'api'], static function () {

    Route::middleware(config('sam-admin.guard-api'))->group(static function () {

        Route::group(['prefix' => 'rts'], static function () {

            Route::group(['prefix' => 'prices'], static function () {
                Route::get('/', [RtsCategoryPriceController::class, 'index'])->middleware('permission:rts.prices.view');
                Route::post('/', [RtsCategoryPriceController::class, 'create'])->middleware('permission:rts.prices.create');
                Route::get('{category}/history', [RtsCategoryPriceController::class, 'history'])->where('category', '[0-9]+')->middleware('permission:rts.prices.history');
                Route::get('categories', [RtsCategoryPriceController::class, 'getCategories'])->middleware('permission:rts.categories.view');
            });

            Route::group(['prefix' => 'containers'], static function () {
                Route::get('/', [RtsContainerController::class, 'index'])->middleware('permission:rts.containers.view');
                Route::get('/{uuid}/form-data', [RtsContainerController::class, 'getDataForForm'])->middleware('permission:rts.containers.view');
                Route::get('{uuid}', [RtsContainerController::class, 'show'])->middleware('permission:rts.containers.show');
                Route::post('/', [RtsContainerController::class, 'create'])->middleware('permission:rts.containers.create');
                Route::put('/{uuid}', [RtsContainerController::class, 'replace'])->middleware('permission:rts.containers.edit');
                Route::delete('/{uuid}', [RtsContainerController::class, 'destroy'])->middleware('permission:rts.containers.delete');
            });

            Route::group(['prefix' => 'locations'], static function () {
                Route::get('/', [RtsLocationController::class, 'index'])->middleware('permission:rts.locations.view');
                Route::get('{uuid}', [RtsLocationController::class, 'show'])->middleware('permission:rts.locations.show');
                Route::post('/', [RtsLocationController::class, 'create'])->middleware('permission:rts.locations.create');
                Route::put('/{uuid}', [RtsLocationController::class, 'replace'])->middleware('permission:rts.locations.edit');
                Route::delete('/{uuid}', [RtsLocationController::class, 'destroy'])->middleware('permission:rts.containers.delete');
            });

            Route::group(['prefix' => 'wastes'], static function () {
                Route::get('/', [RtsWasteTypeController::class, 'index'])->middleware('permission:rts.wastes.view');
                Route::get('/{uuid}', [RtsWasteTypeController::class, 'show'])->middleware('permission:rts.wastes.show');
                Route::post('/', [RtsWasteTypeController::class, 'create'])->middleware('permission:rts.wastes.create');
                Route::put('/{uuid}', [RtsWasteTypeController::class, 'replace'])->middleware('permission:rts.wastes.edit');
                Route::delete('/{uuid}', [RtsWasteTypeController::class, 'destroy'])->middleware('permission:rts.wastes.delete');
            });

            Route::group(['prefix' => 'markers'], static function () {
                Route::get('/', [RtsMarkerController::class, 'index'])->middleware('permission:rts.markers.view');
                Route::get('/{uuid}', [RtsMarkerController::class, 'show'])->middleware('permission:rts.markers.show');
                Route::post('/', [RtsMarkerController::class, 'create'])->middleware('permission:rts.markers.create');
                Route::put('/{uuid}', [RtsMarkerController::class, 'replace'])->middleware('permission:rts.markers.edit');
                Route::delete('/{uuid}', [RtsMarkerController::class, 'destroy'])->middleware('permission:rts.markers.delete');
            });

            Route::group(['prefix' => 'logs'], static function () {
                Route::get('/', [RtsMarkerLogController::class, 'index'])->middleware('permission:rts.logs.view');
                // 3 testy - container nie istnieje, pobranie kontenera bez logow, 3 test ma jakies logi
                Route::get('actions', [RtsMarkerLogController::class, 'indexActions'])->middleware('permission:rts.logs.view');
                Route::post('/', [RtsMarkerLogController::class, 'create'])->middleware('permission:rts.logs.create');
                Route::post('action', [RtsMarkerLogController::class, 'createWithAction'])->middleware('permission:rts.logs.create');
            });
        });
    });
});

<?php

use JanRejnowski\SamRts\App\Enums\Category;

return [

    Category::class => [
        Category::WhiteWastePaper => __('White waste paper'),
        Category::ShreddedPaper => __('Shredded paper'),
        Category::Cardboard => __('Cardboard'),
        Category::WasteMix => __('Waste mix')
    ]
];

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use JanRejnowski\SamRts\Database\Seeds\PermissionSeeder;

class RtsSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        try {
            Schema::create('rts_category_prices', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->decimal('price', 10);
                $table->smallInteger('category');
                $table->softDeletes();
            });

            Schema::create('rts_locations', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->string('name')->unique();
                $table->string('description')->nullable();
                $table->string('image')->nullable();
                $table->softDeletes();
            });

            Schema::create('rts_waste_types', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->string('name')->unique();
                $table->string('description')->nullable();
                $table->smallInteger('category');
                $table->decimal('cost', 10);
                $table->softDeletes();
            });

            Schema::create('rts_markers', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->uuid('location_uuid');
                $table->uuid('waste_type_uuid');
                $table->softDeletes();

                $table->foreign('location_uuid')->references('uuid')->on('rts_locations')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('waste_type_uuid')->references('uuid')->on('rts_waste_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('rts_containers', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->string('name')->unique();
                $table->integer('weight');
                $table->smallInteger('capacity');
                $table->string('serial_number')->nullable()->unique();
                $table->string('description')->nullable();
                $table->softDeletes();
            });

            Schema::create('rts_marker_logs', static function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->timestamps();
                $table->uuid('container_uuid');
                $table->uuid('marker_uuid')->nullable();
                $table->string('action');
                $table->integer('user_id')->unsigned()->nullable();
                $table->integer('weight')->nullable();
                $table->uuid('correct_marker_uuid')->nullable();
                $table->text('description')->nullable();
                $table->string('image')->nullable();
                $table->smallInteger('category')->nullable();
                $table->decimal('price', 10)->nullable();
                $table->decimal('cost', 10)->nullable();
                $table->softDeletes();

                $table->foreign('container_uuid')->references('uuid')->on('rts_containers')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('marker_uuid')->references('uuid')->on('rts_markers')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('correct_marker_uuid')->references('uuid')->on('rts_markers')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
        Artisan::call('db:seed', [
            '--class' => PermissionSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Schema::dropIfExists('rts_marker_logs');
        Schema::dropIfExists('rts_markers');
        Schema::dropIfExists('rts_category_prices');
        Schema::dropIfExists('rts_locations');
        Schema::dropIfExists('rts_waste_types');
        Schema::dropIfExists('rts_containers');
        (new PermissionSeeder())->down();
    }

}

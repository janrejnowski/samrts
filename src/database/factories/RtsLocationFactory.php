<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use JanRejnowski\SamRts\App\Model\RtsLocation;

/**
 * @var $factory Factory
 */
$factory->define(RtsLocation::class, static function (Faker $faker) {

    return [
        'name' => $faker->unique()->domainWord(),
        'description' => $faker->optional(0.3)->text(),
        'image' => $faker->optional(0.3)->text()
    ];

});

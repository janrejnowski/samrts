<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use JanRejnowski\SamRts\App\Model\RtsContainer;

/**
 * @var $factory Factory
 */
$factory->define(RtsContainer::class, static function (Faker $faker) {

    return [
        'name' => $faker->unique()->domainWord,
        'weight' => $faker->randomNumber(3),
        'capacity' => $faker->randomNumber(3),
        'serial_number' => $faker->optional(0.3)->creditCardNumber,
        'description' => $faker->optional()->text()
    ];

});

<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use JanRejnowski\SamRts\App\Enums\Category;
use JanRejnowski\SamRts\App\Model\RtsWasteType;

/**
 * @var $factory Factory
 */
$factory->define(RtsWasteType::class, static function (Faker $faker) {

    return [
        'name' => $faker->unique()->domainWord(),
        'description' => $faker->optional()->text,
        'category' => $faker->randomElement(Category::getValues()),
        'cost' => $faker->randomFloat(2, 0, 100)
    ];

});

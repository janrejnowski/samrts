<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use JanRejnowski\SamRts\App\Model\RtsMarkerLog;

/**
 * @var $factory Factory
 */
$factory->define(RtsMarkerLog::class, static function (Faker $faker) {

    return [
        'weight' => $faker->randomNumber(3),
        'description' => $faker->optional(0.3)->text(),
        'price' => $faker->randomFloat(2, 1, 100),
    ];

});

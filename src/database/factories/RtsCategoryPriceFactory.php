<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use JanRejnowski\SamRts\App\Enums\Category;
use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;

/**
 * @var $factory Factory
 */
$factory->define(RtsCategoryPrice::class, static function (Faker $faker) {

    return [
        'price' => $faker->randomFloat(2, 0, 100),
        'category' => $faker->randomElement(Category::getValues())
    ];

});

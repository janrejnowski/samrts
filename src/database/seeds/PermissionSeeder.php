<?php

namespace JanRejnowski\SamRts\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'rts.prices.view',
                'name' => 'Can View Prices',
                'description' => 'Can view prices',
            ],
            [
                'slug' => 'rts.prices.history',
                'name' => 'Can View Prices History',
                'description' => 'Can view prices history',
            ],
            [
                'slug' => 'rts.prices.create',
                'name' => 'Can Show Assets',
                'description' => 'Can show assets',
            ],
            [
                'slug' => 'rts.categories.view',
                'name' => 'Can View Categories',
                'description' => 'Can view categories',
            ],
            [
                'slug' => 'rts.containers.view',
                'name' => 'Can View Containers',
                'description' => 'Can view containers',
            ],
            [
                'slug' => 'rts.containers.show',
                'name' => 'Can Show Containers',
                'description' => 'Can show containers',
            ],
            [
                'slug' => 'rts.containers.create',
                'name' => 'Can Create Containers',
                'description' => 'Can create containers',
            ],
            [
                'slug' => 'rts.containers.edit',
                'name' => 'Can Edit Containers',
                'description' => 'Can edit containers',
            ],
            [
                'slug' => 'rts.containers.delete',
                'name' => 'Can Delete Containers',
                'description' => 'Can delete containers',
            ],
            [
                'slug' => 'rts.locations.view',
                'name' => 'Can View Locations',
                'description' => 'Can view locations',
            ],
            [
                'slug' => 'rts.locations.show',
                'name' => 'Can Show Locations',
                'description' => 'Can show locations',
            ],
            [
                'slug' => 'rts.locations.create',
                'name' => 'Can Create Locations',
                'description' => 'Can create locations',
            ],
            [
                'slug' => 'rts.locations.edit',
                'name' => 'Can Edit Locations',
                'description' => 'Can edit locations',
            ],
            [
                'slug' => 'rts.locations.delete',
                'name' => 'Can Delete Locations',
                'description' => 'Can delete locations',
            ],
            [
                'slug' => 'rts.wastes.view',
                'name' => 'Can View Waste Types',
                'description' => 'Can view waste types',
            ],
            [
                'slug' => 'rts.wastes.show',
                'name' => 'Can Show Waste Types',
                'description' => 'Can show waste types',
            ],
            [
                'slug' => 'rts.wastes.create',
                'name' => 'Can Create Waste Types',
                'description' => 'Can create waste types',
            ],
            [
                'slug' => 'rts.wastes.edit',
                'name' => 'Can Edit Waste Types',
                'description' => 'Can edit waste types',
            ],
            [
                'slug' => 'rts.wastes.delete',
                'name' => 'Can Delete Waste Types',
                'description' => 'Can delete waste types',
            ],
            [
                'slug' => 'rts.markers.view',
                'name' => 'Can View Markers',
                'description' => 'Can view markers',
            ],
            [
                'slug' => 'rts.markers.show',
                'name' => 'Can Show Markers',
                'description' => 'Can show markers',
            ],
            [
                'slug' => 'rts.markers.create',
                'name' => 'Can Create Markers',
                'description' => 'Can create markers',
            ],
            [
                'slug' => 'rts.markers.edit',
                'name' => 'Can Edit Markers',
                'description' => 'Can edit markers',
            ],
            [
                'slug' => 'rts.markers.delete',
                'name' => 'Can Delete Markers',
                'description' => 'Can delete markers',
            ],
            [
                'slug' => 'rts.logs.view',
                'name' => 'Can View Markers Logs',
                'description' => 'Can view markers logs',
            ],
            [
                'slug' => 'rts.logs.create',
                'name' => 'Can Create Markers Logs',
                'description' => 'Can create markers logs',
            ]
        ];
    }

}

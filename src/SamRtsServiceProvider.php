<?php

namespace JanRejnowski\SamRts;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerLogRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\RtsCategoryPriceRepository;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsLocationRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsWasteTypeRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\RtsContainerRepository;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsCategoryPriceRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsContainerRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\RtsLocationRepository;
use JanRejnowski\SamRts\App\Repositories\RtsMarkerLogRepository;
use JanRejnowski\SamRts\App\Repositories\RtsMarkerRepository;
use JanRejnowski\SamRts\App\Repositories\RtsWasteTypeRepository;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class SamRtsServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-rts';
    }

    public function boot(): void
    {
        $this->loadAndPublish(__DIR__ . '/resources/lang', __DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang/', $this->getNamespace());

        if (File::isDirectory(public_path('storage/rts-images')) === false) {
            File::makeDirectory(public_path('storage/rts-images'), 0777, true, true);
        }
    }

    public function register(): void
    {
        $this->configureBinding([
            RtsContainerRepositoryInterface::class => RtsContainerRepository::class,
            RtsCategoryPriceRepositoryInterface::class => RtsCategoryPriceRepository::class,
            RtsLocationRepositoryInterface::class => RtsLocationRepository::class,
            RtsWasteTypeRepositoryInterface::class => RtsWasteTypeRepository::class,
            RtsMarkerRepositoryInterface::class => RtsMarkerRepository::class,
            RtsMarkerLogRepositoryInterface::class => RtsMarkerLogRepository::class
        ]);
        $this->registerEloquentFactoriesFrom(__DIR__ . '/database/factories');
    }

}

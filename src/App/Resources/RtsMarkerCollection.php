<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsMarker;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsMarkerCollection extends ApiCollection
{
    public $collects = RtsMarker::class;

}

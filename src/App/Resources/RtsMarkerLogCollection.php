<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsMarkerLog;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsMarkerLogCollection extends ApiCollection
{
    public $collects = RtsMarkerLogResource::class;

}

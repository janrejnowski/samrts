<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsContainer;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsContainer $resource
 */
class RtsContainerResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'uuid' => $this->resource->uuid,
            'name' => $this->resource->name,
            'weight' => $this->resource->weight,
            'capacity' => $this->resource->capacity,
            'serial_number' => $this->resource->serial_number,
            'description' => $this->resource->description
        ];
    }

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsCategoryPrice $resource
 */
class RtsCategoryPriceResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'price' => $this->resource->price,
            'category' => $this->resource->category,
            'created_at' => $this->resource->created_at->format('Y-m-d H:i:s')
        ];
    }

}

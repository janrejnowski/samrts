<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsWasteType;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsWasteTypeCollection extends ApiCollection
{
    public $collects = RtsWasteType::class;

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsMarker;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsMarker $resource
 */
class RtsMarkerResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'uuid' => $this->resource->uuid,
            'location_uuid' => $this->resource->location_uuid,
            'location' => $this->whenLoaded('location', function () {
                return $this->resource->location->name;
            }),
            'waste_type_uuid' => $this->resource->waste_type_uuid,
            'waste_type' => $this->whenLoaded('waste_type', function () {
                return $this->resource->waste_type->name;
            }),
        ];
    }

}

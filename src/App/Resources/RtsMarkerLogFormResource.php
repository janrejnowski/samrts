<?php

namespace JanRejnowski\SamRts\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class RtsMarkerLogFormResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'marker_uuid' => $this->resource->marker_uuid,
            'last_action' => $this->resource->last_action,
            'next_action' => $this->resource->next_action
        ];
    }
}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsLocation;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsLocation $resource
 */
class RtsLocationResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'uuid' => $this->resource->uuid,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'image' => $this->resource->image
        ];
    }

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsMarkerLog;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsMarkerLog $resource
 */
class RtsMarkerLogResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'uuid' => $this->resource->uuid,
            'container_uuid' => $this->resource->container_uuid,
            'marker_uuid' => $this->resource->marker_uuid,
            'action' => $this->resource->action,
            'user_id' => $this->resource->user_id,
            'weight' => $this->resource->weight,
            'correct_marker_uuid' => $this->resource->correct_marker_uuid,
            'description' => $this->resource->description,
            'category' => $this->resource->category,
            'price' => $this->resource->price,
            'cost' => $this->resource->cost
        ];
    }

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Enums\Category;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property Category $resource
 */
class RtsCategoryResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'text' => $this->resource->description,
            'value' => $this->resource->value
        ];
    }

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsWasteType;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property RtsWasteType $resource
 */
class RtsWasteTypeResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'uuid' => $this->resource->uuid,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'category' => $this->resource->category,
            'cost' => $this->resource->cost
        ];
    }

}

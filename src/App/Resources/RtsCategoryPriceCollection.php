<?php

namespace JanRejnowski\SamRts\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class RtsCategoryPriceCollection extends ApiCollection
{
    public $collects = RtsCategoryPriceResource::class;

}

<?php

namespace JanRejnowski\SamRts\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class RtsActionResource extends ApiResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->resource->value,
            'key' => $this->resource->key
        ];
    }
}

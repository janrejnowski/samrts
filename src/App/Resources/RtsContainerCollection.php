<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsContainer;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsContainerCollection extends ApiCollection
{
    public $collects = RtsContainer::class;

}

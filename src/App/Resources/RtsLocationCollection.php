<?php

namespace JanRejnowski\SamRts\App\Resources;

use JanRejnowski\SamRts\App\Model\RtsLocation;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsLocationCollection extends ApiCollection
{
    public $collects = RtsLocation::class;

}

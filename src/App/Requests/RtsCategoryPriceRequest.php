<?php

namespace JanRejnowski\SamRts\App\Requests;

use JanRejnowski\SamRts\App\Enums\Category;
use Totem\SamCore\App\Requests\BaseRequest;

class RtsCategoryPriceRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'price' => 'required|numeric|min:0',
            'category' => 'required|enum_value:' . Category::class
        ];
    }

}

<?php

namespace JanRejnowski\SamRts\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class RtsLocationRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|unique:rts_locations',
            'description' => 'nullable',
            'image' => 'nullable'
        ];
    }

}

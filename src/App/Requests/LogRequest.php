<?php

namespace JanRejnowski\SamRts\App\Requests;

use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Totem\SamCore\App\Requests\BaseRequest;

class LogRequest extends BaseRequest
{
    public function rules(): array
    {
        switch ($this->input('action')) {
            case 1:
            {
                return $this->in_tent_rules();
            }
            case 2:
            {
                return $this->in_hall_rules();
            }
            case 3:
            {
                return $this->to_weighting_rules();
            }
            case 4:
            {
                return $this->weighting_rules();
            }
            case 5:
            {
                return $this->out_tent_rules();
            }
            default:
            {
                throw new UnprocessableEntityHttpException(__('Action not recognized.'));
            }
        }
    }

    private function out_tent_rules(): array
    {
        return [
            'container_uuid' => 'required|exists:rts_containers,uuid',
            'action' => 'required'
        ];
    }

    private function in_tent_rules(): array
    {
        return [
            'container_uuid' => 'required|exists:rts_containers,uuid',
            'action' => 'required'
        ];
    }

    private function in_hall_rules(): array
    {
        return [
            'container_uuid' => 'required|exists:rts_containers,uuid',
            'marker_uuid' => 'required|exists:rts_markers,uuid',
            'action' => 'required'
        ];
    }

    private function to_weighting_rules(): array
    {
        return [
            'container_uuid' => 'required|exists:rts_containers,uuid',
            'marker_uuid' => 'required|exists:rts_markers,uuid',
            'action' => 'required'
        ];
    }

    private function weighting_rules(): array
    {
        return [
            'container_uuid' => 'required|exists:rts_containers,uuid',
            'marker_uuid' => 'required|exists:rts_markers,uuid',
            'action' => 'required',
            'user_id' => 'required',
            'weight' => 'required|integer|min:1',
            'correct_marker_uuid' => 'nullable|exists:rts_markers,uuid',
            'description' => 'required_with:correct_marker_uuid',
            'image' => 'required_with:correct_marker_uuid|file|image|max:5000'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->id()
        ]);
    }
}

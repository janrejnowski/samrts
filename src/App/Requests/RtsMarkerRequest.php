<?php

namespace JanRejnowski\SamRts\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class RtsMarkerRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'location_uuid' => 'required|exists:rts_locations,uuid',
            'waste_type_uuid' => 'required|exists:rts_waste_types,uuid'
        ];
    }

}

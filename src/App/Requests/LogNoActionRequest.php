<?php

namespace JanRejnowski\SamRts\App\Requests;

class LogNoActionRequest extends LogRequest
{
    public function rules(): array
    {
        if (!$this->has('action')) {
            return [
                'container_uuid' => 'required|exists:rts_containers,uuid'
            ];
        } else {
            return parent::rules();
        }
    }
}

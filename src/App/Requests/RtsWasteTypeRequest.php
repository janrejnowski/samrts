<?php

namespace JanRejnowski\SamRts\App\Requests;

use JanRejnowski\SamRts\App\Enums\Category;
use Totem\SamCore\App\Requests\BaseRequest;

class RtsWasteTypeRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|unique:rts_waste_types',
            'description' => 'nullable',
            'category' => 'required|enum_value:' . Category::class,
            'cost' => 'required|numeric|min:0'
        ];
    }

}

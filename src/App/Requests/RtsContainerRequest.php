<?php

namespace JanRejnowski\SamRts\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class RtsContainerRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|unique:rts_containers',
            'weight' => 'required|numeric|min:0',
            'capacity' => 'required|numeric|min:0',
            'serial_number' => 'nullable|unique:rts_containers',
            'description' => 'nullable'
        ];
    }

}

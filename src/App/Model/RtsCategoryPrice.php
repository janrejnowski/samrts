<?php

namespace JanRejnowski\SamRts\App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property float price
 * @property int category
 * @property Carbon created_at
 */
class RtsCategoryPrice extends Model
{
    use SoftDeletes,
        HasUuid;

    protected $casts = [
        'price' => 'float'
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'updated_at',
            'deleted_at'
        ]);

        $this->fillable([
            'price',
            'category'
        ]);

        $this->primaryKey = 'uuid';
        parent::__construct($attributes);
    }

}

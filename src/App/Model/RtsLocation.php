<?php

namespace JanRejnowski\SamRts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property string uuid
 * @property string name
 * @property string description
 * @property string image
 */
class RtsLocation extends Model
{
    use SoftDeletes,
        HasUuid;

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at'
        ]);

        $this->fillable([
            'name',
            'description',
            'image'
        ]);

        $this->primaryKey = 'uuid';
        parent::__construct($attributes);
    }

    public function markers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RtsMarker::class);
    }

}

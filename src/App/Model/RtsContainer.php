<?php

namespace JanRejnowski\SamRts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property integer weight
 * @property integer capacity
 * @property string name
 * @property string description
 * @property string serial_number
 * @property string uuid
 */
class RtsContainer extends Model
{
    use SoftDeletes,
        HasUuid;

    protected $casts = [
        'weight' => 'integer',
        'capacity' => 'integer',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at'
        ]);

        $this->fillable([
            'name',
            'weight',
            'capacity',
            'serial_number',
            'description'
        ]);

        $this->primaryKey = 'uuid';
        parent::__construct($attributes);
    }

    public function marker_logs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RtsMarkerLog::class);
    }
}

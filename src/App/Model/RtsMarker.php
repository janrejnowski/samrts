<?php

namespace JanRejnowski\SamRts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property string uuid
 * @property string location_uuid
 * @property string waste_type_uuid
 *
 * @property RtsLocation $location
 * @property RtsWasteType $waste_type
 */
class RtsMarker extends Model
{
    use SoftDeletes,
        HasUuid;

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at'
        ]);

        $this->fillable([
            'location_uuid',
            'waste_type_uuid'
        ]);

        $this->primaryKey = 'uuid';
        parent::__construct($attributes);
    }

    public function location(): BelongsTo
    {
        return $this->belongsTo(RtsLocation::class);
    }

    public function waste_type(): BelongsTo
    {
        return $this->belongsTo(RtsWasteType::class);
    }

    public function marker_logs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RtsMarkerLog::class);
    }

    public function correct_marker_logs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RtsMarkerLog::class);
    }
}

<?php

namespace JanRejnowski\SamRts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\HasFiles;
use Totem\SamCore\App\Traits\HasUuid;
use Totem\SamUsers\App\Model\User;

/**
 * @property string uuid
 * @property string action
 * @property int weight
 * @property string description
 * @property int category
 * @property float price
 * @property float cost
 * @property string image;
 *
 * @property string container_uuid
 * @property string marker_uuid
 * @property int user_id
 * @property string correct_marker_uuid
 */
class RtsMarkerLog extends Model
{
    use SoftDeletes,
        HasUuid,
        HasFiles;

    protected $casts = [
        'price' => 'float',
        'cost' => 'float',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at'
        ]);

        $this->fillable([
            'container_uuid',
            'marker_uuid',
            'action',
            'user_id',
            'weight',
            'correct_marker_uuid',
            'description',
            'category',
            'sell',
            'cost',
        ]);

        $this->primaryKey = 'uuid';
        parent::__construct($attributes);
    }

    public function container(): BelongsTo
    {
        return $this->belongsTo(RtsContainer::class);
    }

    public function marker(): BelongsTo
    {
        return $this->belongsTo(RtsMarker::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function correct_marker(): BelongsTo
    {
        return $this->belongsTo(RtsMarker::class);
    }

}

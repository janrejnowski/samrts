<?php

namespace JanRejnowski\SamRts\App\Traits;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Totem\SamCore\App\Exceptions\RepositoryException;

trait UUIDRepositoryTrait
{
    public function findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*']): Model
    {
        if ($uuid === null) {
            throw new RepositoryException(__('No uuid have been given.'));
        }

        $data = $this->model->with($relationships)->find($uuid, $columns);

        if ($data === null) {
            throw new RepositoryException(
                __(property_exists($this, 'not_found_msg') ? $this->not_found_msg : 'Given uuid :code is invalid.', ['code' => $uuid]),
                404
            );
        }

        return $data;
    }

    public function deleteUUID(string $uuid)
    {
        $model = $this->findWithRelationsByUuid($uuid);

        try {
            $model->delete();
            return $model;
        } catch (HttpException $exception) {
            throw new RepositoryException($exception->getMessage(), $exception->getStatusCode());
        } catch (Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }

}

<?php

namespace JanRejnowski\SamRts\App\Service\Action;

class ActionOutTent implements ActionInterface
{

    public function next(): int
    {
        return \JanRejnowski\SamRts\App\Enums\Action::InTent;
    }
}

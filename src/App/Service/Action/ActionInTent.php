<?php

namespace JanRejnowski\SamRts\App\Service\Action;

class ActionInTent implements ActionInterface
{

    public function next(): int
    {
        return \JanRejnowski\SamRts\App\Enums\Action::InHall;
    }
}

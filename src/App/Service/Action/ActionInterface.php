<?php

namespace JanRejnowski\SamRts\App\Service\Action;

interface ActionInterface
{

    public function next() : int;
}

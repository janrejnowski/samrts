<?php

namespace JanRejnowski\SamRts\App\Service\Action;

class ActionInHall implements ActionInterface
{

    public function next(): int
    {
        return \JanRejnowski\SamRts\App\Enums\Action::ToWeighting;
    }
}

<?php

namespace JanRejnowski\SamRts\App\Service\Action;

class ActionWeighting implements ActionInterface
{

    public function next(): int
    {
        return \JanRejnowski\SamRts\App\Enums\Action::OutTent;
    }
}

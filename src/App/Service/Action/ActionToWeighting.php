<?php

namespace JanRejnowski\SamRts\App\Service\Action;

class ActionToWeighting implements ActionInterface
{

    public function next(): int
    {
        return \JanRejnowski\SamRts\App\Enums\Action::Weighting;
    }
}

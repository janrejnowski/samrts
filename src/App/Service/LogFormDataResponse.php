<?php

namespace JanRejnowski\SamRts\App\Service;

use Illuminate\Contracts\Support\Arrayable;
use JanRejnowski\SamRts\App\Enums\Action;
use JanRejnowski\SamRts\App\Model\RtsMarkerLog;

class LogFormDataResponse implements Arrayable
{
    public ?string $marker_uuid = null;
    public ?int $last_action = null;
    public int $next_action;

    public function __construct(?RtsMarkerLog $lastLog)
    {
        if ($lastLog === null) {
            $this->next_action = Action::InTent;
        } else {
            $lastAction = $lastLog->action;
            $this->last_action = Action::getValue($lastAction);
            $this->next_action = ActionFactory::from($lastAction)->next();
            $this->marker_uuid = $lastLog->marker_uuid;
        }
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}

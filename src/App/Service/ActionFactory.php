<?php

namespace JanRejnowski\SamRts\App\Service;

use JanRejnowski\SamRts\App\Enums\Action;
use JanRejnowski\SamRts\App\Exceptions\InvalidActionException;
use JanRejnowski\SamRts\App\Service\Action\ActionInterface;
use JanRejnowski\SamRts\App\Service\Action\ActionInHall;
use JanRejnowski\SamRts\App\Service\Action\ActionInTent;
use JanRejnowski\SamRts\App\Service\Action\ActionOutTent;
use JanRejnowski\SamRts\App\Service\Action\ActionToWeighting;
use JanRejnowski\SamRts\App\Service\Action\ActionWeighting;

class ActionFactory
{
    public static function from(string $action): ActionInterface
    {
        switch ($action) {
            case Action::getKey(Action::InTent):
                return new ActionInTent();
            case Action::getKey(Action::InHall):
                return new ActionInHall();
            case Action::getKey(Action::ToWeighting):
                return new ActionToWeighting();
            case Action::getKey(Action::Weighting):
                return new ActionWeighting();
            case Action::getKey(Action::OutTent):
                return new ActionOutTent();
            default:
                throw new InvalidActionException('Action not recognized.');
        }
    }
}

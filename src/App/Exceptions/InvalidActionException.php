<?php

namespace JanRejnowski\SamRts\App\Exceptions;

use Throwable;
use InvalidArgumentException;

class InvalidActionException extends InvalidArgumentException
{
    public function __construct($message = '', $code = 400, Throwable $previous = null)
    {
        parent::__construct($message ?: __('Invalid action.'), $code, $previous);
    }
}

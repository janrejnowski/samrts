<?php

namespace JanRejnowski\SamRts\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use Illuminate\Support\Collection;

class Action extends Enum implements LocalizedEnum
{
    public const InTent = 1;
    public const InHall = 2;
    public const ToWeighting = 3;
    public const Weighting = 4;
    public const OutTent = 5;

    public static function toCollection(): Collection
    {
        $array = self::toArray();
        $collection = collect();

        foreach ($array as $key => $value) {
            $collection->push(
                (object)[
                    'value' => $value,
                    'key' => $key,
                    'text' => self::getDescription($value)
                ]
            );
        }

        return $collection;
    }

    public static function getLocalizationKey(): string
    {
        return 'sam-rts::enums.' . __CLASS__;
    }

}

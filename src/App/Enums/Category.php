<?php

namespace JanRejnowski\SamRts\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use Illuminate\Support\Collection;

class Category extends Enum implements LocalizedEnum
{
    public const WhiteWastePaper = 1;
    public const ShreddedPaper = 2;
    public const Cardboard = 3;
    public const WasteMix = 4;

    public static function toCollection(): Collection
    {
        $array = self::toArray();
        $collection = collect();

        foreach ($array as $key => $value) {
            $collection->push(
                (object)[
                    'value' => $value,
                    'key' => $key,
                    'description' => self::getDescription($value)
                ]
            );
        }

        return $collection;
    }

    public static function getLocalizationKey(): string
    {
        return 'sam-rts::enums.' . __CLASS__;
    }

}

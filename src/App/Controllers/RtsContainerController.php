<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Repositories\Contracts\RtsContainerRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerLogRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsContainerRequest;
use JanRejnowski\SamRts\App\Resources\RtsContainerCollection;
use JanRejnowski\SamRts\App\Resources\RtsContainerResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiResource;

class RtsContainerController extends ApiController
{
    private RtsContainerRepositoryInterface $rtsContainerRepository;
    private RtsMarkerLogRepositoryInterface $rtsMarkerLogRepository;

    /**
     * @param RtsContainerRepositoryInterface $rtsContainerRepository
     * @param RtsMarkerLogRepositoryInterface $rtsMarkerLogRepository
     */
    public function __construct(RtsContainerRepositoryInterface $rtsContainerRepository, RtsMarkerLogRepositoryInterface $rtsMarkerLogRepository)
    {
        $this->rtsContainerRepository = $rtsContainerRepository;
        $this->rtsMarkerLogRepository = $rtsMarkerLogRepository;
    }

    public function index(): RtsContainerCollection
    {
        return new RtsContainerCollection($this->rtsContainerRepository->all(['*'], 'uuid'));
    }

    public function show(string $uuid): RtsContainerResource
    {
        return new RtsContainerResource($this->rtsContainerRepository->findWithRelationsByUuid($uuid));
    }

    public function create(RtsContainerRequest $request): RtsContainerResource
    {
        return new RtsContainerResource($this->rtsContainerRepository->storeUUID($request));
    }

    public function replace(string $uuid, RtsContainerRequest $request): RtsContainerResource
    {
        return new RtsContainerResource($this->rtsContainerRepository->storeUUID($request, $uuid));
    }

    public function destroy(string $uuid): RtsContainerResource
    {
        return new RtsContainerResource($this->rtsContainerRepository->deleteUUID($uuid));
    }

    public function getDataForForm(string $uuid): ApiResource
    {
        return new ApiResource($this->rtsMarkerLogRepository->getDataForForm($this->rtsContainerRepository->findWithRelationsByUUID($uuid)));
    }
}

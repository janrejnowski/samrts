<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Repositories\Contracts\RtsWasteTypeRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsWasteTypeRequest;
use JanRejnowski\SamRts\App\Resources\RtsWasteTypeCollection;
use JanRejnowski\SamRts\App\Resources\RtsWasteTypeResource;
use Totem\SamCore\App\Controllers\ApiController;

class RtsWasteTypeController extends ApiController
{
    private RtsWasteTypeRepositoryInterface $rtsWasteTypeRepository;

    public function __construct(RtsWasteTypeRepositoryInterface $rtsWasteTypeRepository)
    {
        $this->rtsWasteTypeRepository = $rtsWasteTypeRepository;
    }

    public function index(): RtsWasteTypeCollection
    {
        return new RtsWasteTypeCollection($this->rtsWasteTypeRepository->all(['*'], 'uuid'));
    }

    public function show(string $uuid): RtsWasteTypeResource
    {
        return new RtsWasteTypeResource($this->rtsWasteTypeRepository->findWithRelationsByUUID($uuid));
    }

    public function create(RtsWasteTypeRequest $request): RtsWasteTypeResource
    {
        return new RtsWasteTypeResource($this->rtsWasteTypeRepository->storeUUID($request));
    }

    public function replace(string $uuid, RtsWasteTypeRequest $request): RtsWasteTypeResource
    {
        return new RtsWasteTypeResource($this->rtsWasteTypeRepository->storeUUID($request, $uuid));
    }

    public function destroy(string $uuid): RtsWasteTypeResource
    {
        return new RtsWasteTypeResource($this->rtsWasteTypeRepository->deleteUUID($uuid));
    }

}

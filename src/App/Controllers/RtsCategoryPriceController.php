<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Enums\Category;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsCategoryPriceRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsCategoryPriceRequest;
use JanRejnowski\SamRts\App\Resources\RtsCategoryPriceCollection;
use JanRejnowski\SamRts\App\Resources\RtsCategoryPriceResource;
use JanRejnowski\SamRts\App\Resources\RtsCategoryResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiCollection;

class RtsCategoryPriceController extends ApiController
{
    private RtsCategoryPriceRepositoryInterface $categoryPriceRepository;

    public function __construct(RtsCategoryPriceRepositoryInterface $categoryPriceRepository)
    {
        $this->categoryPriceRepository = $categoryPriceRepository;
    }

    public function getCategories(): ApiCollection
    {
        return RtsCategoryResource::collection(Category::toCollection());
    }

    public function index(): RtsCategoryPriceCollection
    {
        return new RtsCategoryPriceCollection($this->categoryPriceRepository->allDistinct());
    }

    public function history(int $category): RtsCategoryPriceCollection
    {
        return new RtsCategoryPriceCollection($this->categoryPriceRepository->history($category));
    }

    public function create(RtsCategoryPriceRequest $request): RtsCategoryPriceResource
    {
        return new RtsCategoryPriceResource($this->categoryPriceRepository->store($request));
    }

}

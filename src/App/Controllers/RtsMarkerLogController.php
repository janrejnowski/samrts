<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Enums\Action;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsContainerRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerLogRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\LogNoActionRequest;
use JanRejnowski\SamRts\App\Requests\LogRequest;
use JanRejnowski\SamRts\App\Resources\RtsActionResource;
use JanRejnowski\SamRts\App\Resources\RtsMarkerLogCollection;
use JanRejnowski\SamRts\App\Resources\RtsMarkerLogFormResource;
use JanRejnowski\SamRts\App\Resources\RtsMarkerLogResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Resources\ApiResource;

class RtsMarkerLogController extends ApiController
{
    private RtsMarkerLogRepositoryInterface $rtsMarkerLogRepository;

    public function __construct(RtsMarkerLogRepositoryInterface $rtsMarkerLogRepository)
    {
        $this->rtsMarkerLogRepository = $rtsMarkerLogRepository;
    }

    public function index(): RtsMarkerLogCollection
    {
        return new RtsMarkerLogCollection($this->rtsMarkerLogRepository->all(['*'], 'uuid'));
    }

    public function create(LogNoActionRequest $request): RtsMarkerLogResource
    {
        return new RtsMarkerLogResource($this->rtsMarkerLogRepository->storeAndValidateUUID($request));
    }

    public function createWithAction(LogRequest $request): RtsMarkerLogResource
    {
        return new RtsMarkerLogResource($this->rtsMarkerLogRepository->storeUUID($request));
    }

    public function indexActions(): ApiCollection
    {
        return RtsActionResource::collection(Action::toCollection());
    }
}

<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Repositories\Contracts\RtsLocationRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsLocationRequest;
use JanRejnowski\SamRts\App\Resources\RtsLocationCollection;
use JanRejnowski\SamRts\App\Resources\RtsLocationResource;
use Totem\SamCore\App\Controllers\ApiController;

class RtsLocationController extends ApiController
{
    private RtsLocationRepositoryInterface $rtsLocationRepository;

    public function __construct(RtsLocationRepositoryInterface $rtsLocationRepository)
    {
        $this->rtsLocationRepository = $rtsLocationRepository;
    }

    public function index(): RtsLocationCollection
    {
        return new RtsLocationCollection($this->rtsLocationRepository->all(['*'], 'uuid'));
    }

    public function show(string $uuid): RtsLocationResource
    {
        return new RtsLocationResource($this->rtsLocationRepository->findWithRelationsByUuid($uuid));
    }

    public function create(RtsLocationRequest $request): RtsLocationResource
    {
        return new RtsLocationResource($this->rtsLocationRepository->storeUUID($request));
    }

    public function replace(string $uuid, RtsLocationRequest $request): RtsLocationResource
    {
        return new RtsLocationResource($this->rtsLocationRepository->storeUUID($request, $uuid));
    }

    public function destroy(string $uuid): RtsLocationResource
    {
        return new RtsLocationResource($this->rtsLocationRepository->deleteUUID($uuid));
    }

}

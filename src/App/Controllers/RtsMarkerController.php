<?php

namespace JanRejnowski\SamRts\App\Controllers;

use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsMarkerRequest;
use JanRejnowski\SamRts\App\Resources\RtsMarkerCollection;
use JanRejnowski\SamRts\App\Resources\RtsMarkerResource;
use Totem\SamCore\App\Controllers\ApiController;

class RtsMarkerController extends ApiController
{
    private RtsMarkerRepositoryInterface $rtsMarkerRepository;

    public function __construct(RtsMarkerRepositoryInterface $rtsMarkerRepository)
    {
        $this->rtsMarkerRepository = $rtsMarkerRepository;
    }

    public function index(): RtsMarkerCollection
    {
        return new RtsMarkerCollection($this->rtsMarkerRepository->all(['*'], 'uuid'));
    }

    public function show(string $uuid): RtsMarkerResource
    {
        return new RtsMarkerResource($this->rtsMarkerRepository->findWithRelationsByUUID($uuid, ['waste_type', 'location']));
    }

    public function create(RtsMarkerRequest $request): RtsMarkerResource
    {
        return new RtsMarkerResource($this->rtsMarkerRepository->storeUUID($request));
    }

    public function replace(string $uuid, RtsMarkerRequest $request): RtsMarkerResource
    {
        return new RtsMarkerResource($this->rtsMarkerRepository->storeUUID($request, $uuid));
    }

    public function destroy(string $uuid): RtsMarkerResource
    {
        return new RtsMarkerResource($this->rtsMarkerRepository->deleteUUID($uuid));
    }

}

<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Http\Request;
use JanRejnowski\SamRts\App\Model\RtsMarker;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerRepositoryInterface;
use JanRejnowski\SamRts\App\Traits\UUIDRepositoryTrait;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

class RtsMarkerRepository extends BaseRepository implements RtsMarkerRepositoryInterface, RepositoryInterface
{
    use UUIDRepositoryTrait;

    public string $not_found_msg = 'Given uuid :code is invalid or marker not exist.';

    public function model(): string
    {
        return RtsMarker::class;
    }

    public function storeUUID(Request $request, string $uuid = null): RtsMarker
    {
        /** @var RtsMarker $rts_marker */
        $rts_marker = $uuid === null ? $this->model : $this->findWithRelationsByUuid($uuid);

        $rts_marker->location_uuid = $request->input('location_uuid');
        $rts_marker->waste_type_uuid = $request->input('waste_type_uuid');

        $rts_marker->save();

        return $rts_marker;
    }

}

<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use JanRejnowski\SamRts\App\Model\RtsContainer;
use JanRejnowski\SamRts\App\Model\RtsMarkerLog;
use JanRejnowski\SamRts\App\Requests\LogRequest;

/**
 * @method RtsMarkerLog findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsMarkerLogRepositoryInterface
{
    public function storeAndValidateUUID(LogRequest $request);

    public function getDataForForm(RtsContainer $container);
}

<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use JanRejnowski\SamRts\App\Model\RtsLocation;

/**
 * @method RtsLocation findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsLocationRepositoryInterface extends UUIDRepositoryInterface
{

}

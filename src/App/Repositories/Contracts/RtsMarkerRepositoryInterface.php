<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use JanRejnowski\SamRts\App\Model\RtsMarker;

/**
 * @method RtsMarker findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsMarkerRepositoryInterface extends UUIDRepositoryInterface
{

}

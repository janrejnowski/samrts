<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface UUIDRepositoryInterface
{

    public function storeUUID(Request $request, string $uuid = null);

    /**
     * @param string|null $uuid
     * @param array $relationships
     * @param array $columns
     * @return  Model
     */
    public function findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*']): Model;

    /**
     * @param string $uuid
     * @return  Collection|bool
     */
    public function deleteUUID(string $uuid);

}

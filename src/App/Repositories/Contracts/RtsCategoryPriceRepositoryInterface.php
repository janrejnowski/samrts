<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;
use JanRejnowski\SamRts\App\Requests\RtsCategoryPriceRequest;
use Totem\SamCore\App\Repositories\Contracts\SimpleRepositoryInterface;

/**
 * @method RtsCategoryPrice findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsCategoryPriceRepositoryInterface extends SimpleRepositoryInterface
{

    public function allDistinct(array $columns = ['*']);

    public function store(RtsCategoryPriceRequest $request);

    public function history(int $category, array $columns = ['*']);

    public function getLatestPrice(int $category);

}

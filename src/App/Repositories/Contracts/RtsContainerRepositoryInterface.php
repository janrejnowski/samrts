<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use JanRejnowski\SamRts\App\Model\RtsContainer;

/**
 * @method RtsContainer findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsContainerRepositoryInterface extends UUIDRepositoryInterface
{

}

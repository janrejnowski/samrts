<?php

namespace JanRejnowski\SamRts\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use JanRejnowski\SamRts\App\Model\RtsWasteType;

/**
 * @method RtsWasteType findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
interface RtsWasteTypeRepositoryInterface extends UUIDRepositoryInterface
{

}

<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use JanRejnowski\SamRts\App\Model\RtsWasteType;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsWasteTypeRepositoryInterface;
use JanRejnowski\SamRts\App\Traits\UUIDRepositoryTrait;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

class RtsWasteTypeRepository extends BaseRepository implements RtsWasteTypeRepositoryInterface, RepositoryInterface
{
    use UUIDRepositoryTrait;

    public string $not_found_msg = 'Given uuid :code is invalid or waste type not exist.';

    public function model(): string
    {
        return RtsWasteType::class;
    }

    public function storeUUID(Request $request, string $uuid = null): RtsWasteType
    {
        /** @var RtsWasteType $waste_type */
        $waste_type = $uuid === null ? $this->model : $this->findWithRelationsByUuid($uuid);

        $waste_type->name = $request->input('name');
        $waste_type->description = $request->input('description');
        $waste_type->category = $request->input('category');
        $waste_type->cost = $request->input('cost');

        $waste_type->save();

        return $waste_type;
    }

}

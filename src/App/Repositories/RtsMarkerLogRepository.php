<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use JanRejnowski\SamRts\App\Enums\Action;

use JanRejnowski\SamRts\App\Model\RtsContainer;
use JanRejnowski\SamRts\App\Model\RtsMarker;
use JanRejnowski\SamRts\App\Model\RtsMarkerLog;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsCategoryPriceRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerLogRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsMarkerRepositoryInterface;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsWasteTypeRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\LogRequest;
use JanRejnowski\SamRts\App\Service\ActionFactory;
use JanRejnowski\SamRts\App\Service\LogFormDataResponse;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

/**
 * @method RtsMarkerLog findWithRelationsByUUID(string $uuid = null, array $relationships = [], array $columns = ['*'])
 */
class RtsMarkerLogRepository extends BaseRepository implements RepositoryInterface, RtsMarkerLogRepositoryInterface
{
    private RtsMarkerRepositoryInterface $markerRepository;
    private RtsWasteTypeRepositoryInterface $wasteTypeRepository;
    private RtsCategoryPriceRepositoryInterface $categoryPriceRepository;

    public function model(): string
    {
        return RtsMarkerLog::class;
    }

    public function __construct(RtsMarkerRepositoryInterface    $markerRepository,
                                RtsWasteTypeRepositoryInterface $wasteTypeRepository, RtsCategoryPriceRepositoryInterface $categoryPriceRepository)
    {
        parent::__construct();

        $this->markerRepository = $markerRepository;
        $this->wasteTypeRepository = $wasteTypeRepository;
        $this->categoryPriceRepository = $categoryPriceRepository;
    }

    public function storeUUID(Request $request, string $uuid = null): RtsMarkerLog
    {
        /** @var RtsMarkerLog $rts_marker_log */
        $rts_marker_log = $this->model;
        $rts_marker_log->container_uuid = $request->input('container_uuid');
        $rts_marker_log->action = Action::getKey($request->input('action'));
        $rts_marker_log->marker_uuid = $request->input('marker_uuid');

        if ($request->has('marker_uuid') && $request->has('weight')) {
            $marker = $this->markerRepository->findWithRelationsByUUID($rts_marker_log->marker_uuid);
            if ($request->has('correct_marker_uuid')) {
                $marker = $this->markerRepository->findWithRelationsByUUID($request->input('correct_marker_uuid'));
                $rts_marker_log->correct_marker_uuid = $request->input('correct_marker_uuid');
                $rts_marker_log->description = $request->input("description");
                $rts_marker_log->image = $request->file('image')->store('rts-images', 'public');
            }

            $this->bindMarkerData($marker, $rts_marker_log, $request);
        }

        $rts_marker_log->save();

        return $rts_marker_log;
    }


    private function bindMarkerData(RtsMarker $marker, RtsMarkerLog $rts_marker_log, Request $request): void
    {
        $waste_type = $this->wasteTypeRepository->findWithRelationsByUUID($marker->waste_type_uuid);
        $price = $this->categoryPriceRepository->getLatestPrice($waste_type->category);
        $rts_marker_log->category = $waste_type->category;
        $rts_marker_log->cost = $waste_type->cost;
        $rts_marker_log->price = $price;
        $rts_marker_log->user_id = $request->input('user_id');
        $rts_marker_log->weight = $request->input('weight');
    }

    public function storeAndValidateUUID(LogRequest $request): RtsMarkerLog
    {
        $action = Action::InTent;
        $lastAction = $this->getLatestActionForContainer($request->input('container_uuid'));
        if ($lastAction !== null) {
            $action = ActionFactory::from($lastAction)->next();
        }
        $request->merge([
            'action' => $action
        ]);
        $request->validate($request->rules());
        return $this->storeUUID($request);
    }

    public function getDataForForm(RtsContainer $container): LogFormDataResponse
    {
        return new LogFormDataResponse($this->getLatestLogForContainer($container->uuid));
    }

    private function getLatestActionForContainer(string $container_uuid): ?string
    {
        $lastLog = $this->getLatestLogForContainer($container_uuid);
        return $lastLog !== null ? $lastLog->action : null;
    }

    /**
     * @param string $container_uuid
     * @return null|RtsMarkerLog|Model
     */
    private function getLatestLogForContainer(string $container_uuid): ?RtsMarkerLog
    {
        return $this->model->orderBy('created_at', 'desc')
                           ->firstWhere('container_uuid', $container_uuid);
    }
}

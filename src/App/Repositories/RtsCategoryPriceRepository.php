<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use JanRejnowski\SamRts\App\Model\RtsCategoryPrice;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsCategoryPriceRepositoryInterface;
use JanRejnowski\SamRts\App\Requests\RtsCategoryPriceRequest;
use Totem\SamCore\App\Exceptions\RepositoryException;

class RtsCategoryPriceRepository implements RtsCategoryPriceRepositoryInterface
{
    /**
     * @var Model|Builder
     */
    protected $model;

    public function __construct()
    {
        $this->makeModel();
    }

    public function model(): string
    {
        return RtsCategoryPrice::class;
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function setModel(string $modelClass)
    {
        $this->makeModel($modelClass);

        return $this;
    }

    protected function makeModel(string $className = null)
    {
        return $this->model = app()->make($className ?? $this->model());
    }

    public function allDistinct(array $columns = ['*'])
    {
        $sub = $this->model
            ->select('category', new Expression('MAX(created_at)'))
            ->groupBy('category')
            ->toSql();

        return $this->model
            ->whereRaw('(category, created_at) in (' . $sub . ')')
            ->get();
    }

    public function store(RtsCategoryPriceRequest $request)
    {
        $category_price = $this->model;

        $category_price->fill($request->validated());

        $category_price->save();

        return $category_price;
    }

    public function history(int $category = 0, array $columns = ['*'])
    {
        if ($category === 0) {
            throw new RepositoryException(__('No category id have been given.'));
        }

        return $this->model
            ->where('category', $category)
            ->orderBy('created_at', 'desc')->get($columns);
    }

    public function getLatestPrice(int $category)
    {
        return $this->allDistinct()
                    ->where('category', $category)
                    ->first()
                    ->price;
    }
}

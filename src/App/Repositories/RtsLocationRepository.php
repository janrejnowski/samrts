<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Http\Request;
use JanRejnowski\SamRts\App\Model\RtsLocation;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsLocationRepositoryInterface;
use JanRejnowski\SamRts\App\Traits\UUIDRepositoryTrait;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

class RtsLocationRepository extends BaseRepository implements RtsLocationRepositoryInterface, RepositoryInterface
{
    use UUIDRepositoryTrait;

    public string $not_found_msg = 'Given uuid :code is invalid or location not exist.';

    public function model(): string
    {
        return RtsLocation::class;
    }

    public function storeUUID(Request $request, string $uuid = null): RtsLocation
    {
        /** @var RtsLocation $rts_location */
        $rts_location = $uuid === null ? $this->model : $this->findWithRelationsByUuid($uuid);

        $rts_location->name = $request->input('name');
        $rts_location->description = $request->input('description');
        $rts_location->image = $request->input('image');

        $rts_location->save();

        return $rts_location;
    }

}

<?php

namespace JanRejnowski\SamRts\App\Repositories;

use Illuminate\Http\Request;
use JanRejnowski\SamRts\App\Model\RtsContainer;
use JanRejnowski\SamRts\App\Repositories\Contracts\RtsContainerRepositoryInterface;
use JanRejnowski\SamRts\App\Traits\UUIDRepositoryTrait;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

class RtsContainerRepository extends BaseRepository implements RtsContainerRepositoryInterface, RepositoryInterface
{
    use UUIDRepositoryTrait;

    public string $not_found_msg = 'Given uuid :code is invalid or container not exist.';

    public function model(): string
    {
        return RtsContainer::class;
    }

    public function storeUUID(Request $request, string $uuid = null): RtsContainer
    {
        /** @var RtsContainer $rts_container */
        $rts_container = $uuid === null ? $this->model : $this->findWithRelationsByUuid($uuid);

        $rts_container->name = $request->input('name');
        $rts_container->weight = $request->input('weight');
        $rts_container->capacity = $request->input('capacity');
        $rts_container->serial_number = $request->input('serial_number');
        $rts_container->description = $request->input('description');

        $rts_container->save();

        return $rts_container;
    }

}
